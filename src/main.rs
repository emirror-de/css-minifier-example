extern crate futures;
extern crate serde;
extern crate surf;

use futures::executor::block_on;
use serde::{Serialize, Deserialize};
use std::fs;

/// The struct Body is being send as the request body
#[derive(Serialize, Deserialize)]
struct Body {
    input: String
}

/// Reads the given css file and return it as string
fn read_css(input_filename: String) -> String {
    fs::read_to_string(input_filename).unwrap()
}

/// The actual request method.
/// Sends the POST request and prints the response.
async fn minify_css(css: String) {
    let request_url = "https://cssminifier.com/raw";
    let response = surf::post(request_url)
        .body_form(&Body {input: css})
        .unwrap()
        .recv_string()
        .await;

    println!("{}", response.unwrap());
}

/// Main function that calls the request method.
fn main() {
    let input_filename = "styles.css";
    let request = minify_css(read_css(input_filename.to_string()));
    block_on(request);
}
